#Author: Andraž P.

from keras.layers import Embedding, Dense, LSTM, Dropout, GlobalMaxPooling1D, Input
from keras import Model
from keras.optimizers import Adam
from keras.layers.merge import concatenate


def build_model(bow_features, maxlen, vocab_size, num_classes, embedding_matrix, embedding_dim):
    """Model of a two-input neural network for binary text classification. First input is a matrix of manually crafted
    features, second input is a sequence matrix fed to an embedding layer with pretrained embeddings.

    INPUTS: bow_features - shape of manually crafted features
             maxlen - length of word sequences
             vocab_size - size of the vocabulary for word sequences
             num_classes - number of classes to predict; the model was designed for binary classification problems,
             therefore the recommended number of classes is 2
             embedding_matrix - pretrained weights for embeddings
             embedding_dim - dimension of pretrained embeddings

    OUTPUT: model - returns a compiled Keras model
    """

    inputs = []
    optimizer = Adam(lr=0.001)

    # manually created feature matrix input
    tfidf_matrix = Input(shape=(bow_features,))
    inputs.append(tfidf_matrix)

    # word sequence input
    word_sequence_input = Input(shape=(maxlen,))
    inputs.append(word_sequence_input)
    word_embedding = Embedding(vocab_size + 1, 
                               embedding_dim,
                               weights=[embedding_matrix],
                               input_length=maxlen,
                               trainable=True)(word_sequence_input)
    lstm_layer = LSTM(120, return_sequences=True, dropout=0.4, recurrent_dropout=0.4)(word_embedding)
    pooling = GlobalMaxPooling1D()(lstm_layer)
    word_sequence = Dropout(0.5)(pooling)

    merged = concatenate([tfidf_matrix, word_sequence])
    dense_layer = Dense(150, activation='relu')(merged)
    droput2 = Dropout(0.5)(dense_layer)
    dense_out = Dense(num_classes, activation='sigmoid')(droput2)
    model = Model(inputs=inputs, outputs=dense_out)
    model.summary()
    model.compile(loss='binary_crossentropy',
                  optimizer=optimizer,
                  metrics=['acc'])
    return model

