#Author: Andraž P.

from keras.layers import Embedding, Dense, LSTM, Dropout, GlobalMaxPooling1D, Input
from keras import Model
from keras.optimizers import Adam
from keras.layers.merge import concatenate


def build_model(bow_features, maxlen, vocab_size, num_classes, embedding_matrix, embedding_dim,
                pos_vocab_size):
    """Model of a three-input neural network for binary text classification. First input is a
    matrix of manually crafted features, second input is a word sequence matrix fed to an
    embedding layer with pretrained embeddings, third input is a part-of-speech tag sequence
    matrix fed to the same embedding layer.
    INPUTS: bow_features - shape of manually crafted features
            maxlen - length of word and POS tag sequences
            vocab_size - size of the vocabulary for word sequences
            num_classes - number of classes to predict
            embedding_matrix - pretrained weights for embeddings
            embedding_dim - dimension of pretrained embeddings
            pos_vocab_size - size of the part-of-speech tags vocabulary
    OUTPUT: model - returns a compiled Keras model
    """
    inputs = []
    optimizer = Adam(lr=0.001)

    # manually crafted features input
    tfidf_matrix = Input(shape=(bow_features,))
    inputs.append(tfidf_matrix)

    # word sequences
    word_sequence_input = Input(shape=(maxlen,))
    inputs.append(word_sequence_input)
    word_embedding = Embedding(vocab_size + 1, 
                               embedding_dim,
                               weights=[embedding_matrix],
                               input_length=maxlen,
                               trainable=True)(word_sequence_input)
    word_lstm_layer = LSTM(120, return_sequences=True, dropout=0.4, recurrent_dropout=0.4)\
        (word_embedding)
    word_pooling = GlobalMaxPooling1D()(word_lstm_layer)
    word_sequence = Dropout(0.5)(word_pooling)

    # POS tag sequences
    char_sequence_input = Input(shape=(maxlen, ))
    inputs.append(char_sequence_input)
    char_embedding = Embedding(pos_vocab_size, 128)(char_sequence_input)
    char_lstm_layer = LSTM(120, return_sequences=True, dropout=0.4, recurrent_dropout=0.4)\
        (char_embedding)
    char_pooling = GlobalMaxPooling1D()(char_lstm_layer)
    char_sequence = Dropout(0.5)(char_pooling)

    merged = concatenate([tfidf_matrix, word_sequence, char_sequence])
    dense_layer = Dense(150, activation='relu')(merged)
    droput2 = Dropout(0.5)(dense_layer)
    dense_out = Dense(num_classes, activation='sigmoid')(droput2)
    model = Model(inputs=inputs, outputs=dense_out)
    model.summary()
    model.compile(loss='binary_crossentropy',
                  optimizer=optimizer,
                  metrics=['acc'])
    return model

