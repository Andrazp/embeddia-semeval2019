#Author: Andraž P.

from nltk.corpus import stopwords
import os
import pandas as pd
from nltk.tokenize.treebank import TreebankWordTokenizer
from nltk.stem.snowball import SnowballStemmer
import requests
import json
import numpy as np
from scipy.sparse import hstack
from sklearn.preprocessing import Normalizer
import string
import copy
import nltk
from nltk.tokenize import TweetTokenizer


def build_features(tfidf_matrix, data, data_column):
    """Builds all manually crafted features and adds them to the premade tf-idf matrix. 
    The final matrix is normalized before it is returned.
    INPUTS: tfidf_matrix - tf-idf bag-of-words matrix
            data - dataframe; contains the dataset
            data_column - string; name of the column in the dataset that contains data
    OUTPUT: the final normalized tf-idf matrix with added manually crafted features
    """

    response = sentiment_request(data, data_column)
    encoded_sentiment = encode_sentiment_labels(response)
    sentiment_vector = np.array(encoded_sentiment)
    sentiment_vector = np.reshape(sentiment_vector, (-1, 1))

    tfidf_matrix = hstack((tfidf_matrix, sentiment_vector))

    insults_vector = presence_of_insults(data, data_column)
    insults_vector = np.reshape(insults_vector, (-1, 1))

    tfidf_matrix = hstack((tfidf_matrix, insults_vector))

    for pmark in string.punctuation:
        longest_subsequence_pmark = longest_subsequence(data, pmark, data_column)
        longest_subsequence_pmark = np.reshape(longest_subsequence_pmark, (-1, 1))
        tfidf_matrix = hstack((tfidf_matrix, longest_subsequence_pmark))

    print("Shape of the final matrix:")
    print(tfidf_matrix.shape)

    tfidf_matrix_normalized = Normalizer().fit_transform(tfidf_matrix)
    return tfidf_matrix_normalized


def sentiment_request(data, label):
    """Returns the sentiment label of each document as predicted by a pretrained SVM (Mozetič I,
     Grčar M, Smailović J (2016) Multilingual Twitter Sentiment Classification: The Role of
     Human Annotators. PLoS ONE 11(5): e0155036. https://doi.org/10.1371/journal.pone.0155036).
    INPUTS: data - a dataframe containing the dataset
            label - string, name of the column containing data for processing
    OUTPUT: processed data and associated labels in json format.
    """

    strings = data[label].tolist()
    # removing double quotes so they do not impact conversion to json format
    strings = remove_quotes(strings)

    url = 'http://annotate.ijs.si/TextModels/api/Sentiment/Prediction/model-SvmTwoPlaneBW1-' \
          'English'
    payload = json.dumps(strings)
    headers = {'Content-Type': "application/json"}
    response = requests.request("POST", url, headers=headers, data=payload)

    print("Status code:")
    print(response.status_code)
    return response.json()


def encode_sentiment_labels(data):
    """Maps the sentiment labels to numerical values. Positive = 1, neutral = 0, negative = -1.
    INPUT: data - data and associated labels in json format.
    OUTPUT: list of encoded sentiment labels.
    """

    sentiment_encoding = []
    for entry in data:
        if entry['Label'] == 'Negative':
            sentiment_encoding.append(-1)
        elif entry['Label'] == 'Neutral':
            sentiment_encoding.append(0)
        elif entry['Label'] == 'Positive':
            sentiment_encoding.append(1)

    return sentiment_encoding


def remove_quotes(corpus):
    """Removes double quotes in each document of the dataset. Used as a preprocessing step
    before sentiment prediction.
    INPUT: corpus - list of document to be processed
    OUTPUT: list of processed documents.
    """

    processed_corpus = []
    for doc in corpus:
        char_list = [x for x in doc]
        for char in char_list[:]:
            if char == '"':
                char_list.remove(char)
        document = ''.join(char_list)
        processed_corpus.append(document)

    return processed_corpus


def longest_subsequence(data, character, data_column):
    """Finds the length of the longest sequence of a character in a tweet.
    INPUTS: data - dataframe containing the dataset
            character - the character whose longest sequence the function will output for each
            document in the dataset
            data_column - name of the column of the dataset that contains data to be processed
    OUTPUT: a list of lengths of longest sequences of characters"""

    longest_subsequences = []
    for index, row in data.iterrows():
        counter = 0
        tmp_counter = 0
        for char in row[data_column]:
            if char == character:
                tmp_counter += 1
            else:
                if tmp_counter > counter:
                    counter = tmp_counter
                tmp_counter = 0
        longest_subsequences.append(counter)
    return longest_subsequences


def presence_of_insults(data, data_column):
    """Returns the number of insults in a document based on a English insults list
    (http://metadataconsulting.blogspot.com/2018/09/Google-Facebook-Office-365-Dark-Souls-Bad-
    Offensive-Profanity-key-word-List-2648-words.html)
    INPUTS: data - dataframe containing the dataset
            data_column - string, name of the column of the dataset that contains data to be
            processed
    OUTPUT: list containing number of insults for each document in the dataset
    """

    path = os.path.join(os.path.dirname(__file__), os.pardir)
    insults_file_path = os.path.join(path, 'training-v1/insults.csv')

    insults_df = pd.read_csv(insults_file_path)
    insults_list = insults_df['insult'].tolist()

    data_copy = copy.deepcopy(data)

    tokenizer = TreebankWordTokenizer()
    for index, row in data_copy.iterrows():
        tokens = tokenizer.tokenize(row[data_column])
        row[data_column] = tokens

    insults_vector = []
    for index, row in data_copy.iterrows():
        counter = 0
        for token in row[data_column]:
            if token in insults_list:
                counter += 1
        insults_vector.append(counter)

    #print(insults_vector)
    return insults_vector


def prepare_embedding_matrix(file, embedding_dim, word_index):
    """Maps the vocabulary of the dataset to English GloVe embeddings pretrained on Twitter data.
    (Jeffrey Pennington, Richard Socher, and Christopher D. Manning. 2014. GloVe: Global Vectors
     for Word Representation).
     INPUTS: file - file with pretrained embeddings
             embedding_dim - the dimension of embeddings
             word_index - vocabulary appearing in the dataset
    OUTPUTS: matrix of embeddings"""
    
    embedding_index = {}
    f = open(file, encoding='utf-8')
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embedding_index[word] = coefs
    f.close()

    embedding_matrix = np.zeros((len(word_index) + 1, embedding_dim))
    for word, i in word_index.items():
        embedding_vector = embedding_index.get(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector

    return embedding_matrix


def get_max_text_len(train_data, data_column):
    """Returns the length of the longest document in the dataset.
    INPUTS: train_data - dataframe, the dataset
            data_column - string, name of the column containing the documents
    OUTPUTS: length of the longest document in the dataset."""
    
    train_data_copy = copy.deepcopy(train_data)

    tokenizer = TreebankWordTokenizer()
    for index, row in train_data_copy.iterrows():
        tokens = tokenizer.tokenize(row[data_column])
        #print(tokens)
        row[data_column] = tokens

    print(train_data_copy)
    max_text_len = max([len(text) for text in train_data_copy[data_column].tolist()])
    return max_text_len


def pos_tagging(sentence_list):
    """Creates a list of part-os-speech tags for every document present in the dataset
    INPUTS: sentence_list - list; documents in the dataset
    OUTPUTS: list; lists of POS tags for every document"""

    tokenizer = TweetTokenizer()
    sentences_tokenized = []
    for tweet in sentence_list:
        sentences_tokenized.append(tokenizer.tokenize(tweet))

    sentences_pos = []
    for tweet in sentences_tokenized:
        sentences_pos.append(nltk.pos_tag(tweet))
    
    print(sentences_pos)

    pos_final = []
    for tweet in sentences_pos:
        pos_tags = []
        for tag in tweet:
            pos_tags.append(tag[1])
        pos_final.append(pos_tags)

    return pos_final


def build_pos_vocab(train_data):
    """Maps every part-of-speech tag present in the tagged dataset to a unique integer.
    INPUTS: train_data - dataframe; the tagged dataset.
    OUTPUTS: dictionary; tag:integer.
    """
    # Integer 0 is reserved for padding, integer 1 is reserved by Keras for tags that are
    # not present in the training dataset.
    counter = 2
    pos_vocab = {}
    for tweet in train_data:
        for pos in tweet:
            if pos not in pos_vocab:
                pos_vocab[pos] = counter
                counter += 1
    return pos_vocab


def make_pos_vec(data, pos_vocab, max_text_len):
    """Encodes the tagged dataset.
    INPUTS: data - dataframe; the tagged dataset
            pos_vocab - dictionary; map of tags to unique integers
            max_text_len - integer; maximum length of documents in the dataset
    OUTPUTS: list; vectors of encoded tags for every document in the dataset
    """

    output_pos_tags = np.zeros((len(data), max_text_len), dtype='int32')
    for i in range(len(data)):
        for j in range(max_text_len):
            if j < len(data[i]):
                if data[i][j] in pos_vocab:
                    output_pos_tags[i][j] = pos_vocab[data[i][j]]
                else:
                    output_pos_tags[i][j] = 1

    return output_pos_tags

